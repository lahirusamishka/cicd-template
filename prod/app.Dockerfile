FROM template-npm_runner-prod:hot as npm_runner

FROM template-composer_runner-prod:hot as composer_runner

FROM php:8.0.0-fpm-alpine3.12
WORKDIR /var/www/html/
RUN docker-php-ext-install pdo_mysql
RUN apk add --no-cache \
      freetype-dev \
      libjpeg-turbo-dev \
      php-gd \
    && docker-php-ext-configure gd \
      --with-freetype=/usr/include/ \
      --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-enable gd \
    && apk del --no-cache \
      freetype-dev \
      libjpeg-turbo-dev \
      libpng-dev \
    && rm -rf /tmp/*
COPY --from=composer_runner /app/ .
COPY --from=npm_runner /app/public/ ./public/
RUN apk add --no-cache $PHPIZE_DEPS && pecl install xdebug-3.0.2 && docker-php-ext-enable xdebug
RUN apk --update add git
RUN apk add --no-cache \
      libzip-dev \
      zip \
    && docker-php-ext-install zip
RUN sed -E -i -e 's/max_execution_time = 30/max_execution_time = 120/' /usr/local/etc/php/php.ini-production \
    && sed -E -i -e 's/memory_limit = 128M/memory_limit = 1024M/' /usr/local/etc/php/php.ini-production \
    && cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
