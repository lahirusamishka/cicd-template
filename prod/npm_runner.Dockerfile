FROM node:18.11.0-alpine3.16
WORKDIR /app/
COPY ./src/ .
RUN npm install npm@latest -g
RUN apk add g++ make python3
RUN npm install
RUN npm ci
RUN npm run build
