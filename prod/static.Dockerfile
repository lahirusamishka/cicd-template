FROM template-npm_runner-prod:hot as npm-runner

FROM template-composer_runner-prod:hot as composer-runner

FROM nginx:1.19.6-alpine
WORKDIR /usr/share/nginx/html/
COPY --from=composer-runner /app/public/ .
COPY --from=npm-runner /app/public/ .
