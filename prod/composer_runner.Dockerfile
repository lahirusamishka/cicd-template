FROM php:8.0.0-fpm-alpine3.12
WORKDIR /app/
RUN apk --update add composer
RUN apk --update add git
COPY ./src/ .
RUN composer install --no-dev --ignore-platform-reqs
